from mininet.topo import Topo
from mininet.node import Controller, RemoteController
from mininet.cli import CLI
from mininet.net import Mininet
from mininet.link import Link, Intf, TCLink
from mininet.util import irange, dumpNodeConnections
from mininet.log import setLogLevel, info
import os

class hotel_topo(Topo):
	def __init__(self, sw_num = 3, net_num = 5, **opts):
		super(hotel_topo, self).__init__(**opts)
		ho_num = (sw_num-1)*net_num
		hd2 = int(ho_num/2)
		admin = self.addHost('admin', ip = '10.0.1.2')
		ext = self.addHost('ext_int', ip = '10.1.1.2')
		Switch = [None]*sw_num
		Host = [None]*ho_num
		
		for i in range(sw_num):
		    dpid_str = ('00:00:00:00:00:00:00:%0.2X'%(i+1))
		    Switch[i] = self.addSwitch('Sw%d'%(i+1), dpid = dpid_str, protocols = 'OpenFlow13')
		    if i > 0:
		        self.addLink(Switch[0], Switch[i], i + 1, 1)
		
		for i in range(1, sw_num):
		    for j in range(i+1, sw_num):
		        self.addLink(Switch[i], Switch[j], j+1, i+1)
		
		self.addLink(admin, Switch[0], 1, sw_num+1)
		self.addLink(ext, Switch[0], 1, 1)
	    
		for i in range(sw_num-1):
		    for j in range(net_num):
		        Host[i*net_num+j] = self.addHost('h%d'%(i*net_num+j+1), ip = '10.0.%d.%d'%(i+2, j+2))
		        self.addLink(Host[i*net_num+j], Switch[i+1], 1, j+sw_num+1)
topos = {'test' :(lambda:hotel_topo())}		        
if __name__ == '__main__'	:
	setLogLevel('info')
	#nets = int(input("customer of each type:"));
	#sws = int(input("number of switch:"))
	topo = hotel_topo()
	net = Mininet(topo, link = TCLink, controller = None,\
			autoSetMacs = True, autoStaticArp = True)
	net.addController('controller', controller = RemoteController,\
			ip = "127.0.0.1", port = 6633, protocols = "OpenFlow13")
	net.start()
	
	CLI(net)
	
	

	#dumpNodeConnections(net.hosts)
	#net.pingAll()
	net.stop()