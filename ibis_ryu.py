from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import ether_types

class SimpleSwitch13(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]
    
    def __init__(self, k = 4, *args, **kwargs):
        super(SimpleSwitch13, self).__init__(*args, **kwargs)
        self.mac_to_port = {}
        self.c = int(input("customer of each type:"))
        self.s = int(input("number of switch:"))

    def add_flow(self, datapath, priority, match, actions, tableid, buffer_id=None):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]
        if buffer_id:
            mod = parser.OFPFlowMod(datapath=datapath, buffer_id=buffer_id,
                                    priority=priority, match=match,
                                    instructions=inst)
        else:
            mod = parser.OFPFlowMod(datapath=datapath, priority=priority,\
                                    match=match, instructions=inst, cookie = 0,\
                                    command=ofproto.OFPFC_ADD, idle_timeout=0, \
                                    hard_timeout=0, flags=ofproto.OFPFF_SEND_FLOW_REM,\
                                    table_id = tableid)
        datapath.send_msg(mod)
        #print(mod, "\n")
    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):
        datapath = ev.msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        match = parser.OFPMatch()
        actions = []
        self.add_flow(datapath, 0, match, actions, 0)
		
        dpid = str(hex(datapath.id))[2:].zfill(16)
        self.logger.info("%s", dpid)
        pos = int(dpid[14:], 16)
        print(str(datapath.id))
        #add table 0
        if pos == 1:
            match = parser.OFPMatch(eth_type=0x800,ipv4_dst = ("10.0.1.2", "255.255.255.255"))
            actions = [parser.OFPActionOutput(self.s+1,0)]
            self.add_flow(datapath, 1, match, actions, 0)
            for i in range(1,self.s+1):
                if i != pos:
                    match = parser.OFPMatch(eth_type=0x800,ipv4_dst = ("10.0.%d.0"%i, "255.255.255.0"))
                    actions = [parser.OFPActionOutput(i,0)]
                    self.add_flow(datapath, 1, match, actions, 0)
            
        else:
            match = parser.OFPMatch(eth_type=0x800,ipv4_dst = ("10.0.1.2", "255.255.255.255"))
            actions = [parser.OFPActionOutput(1,0)]
            self.add_flow(datapath, 1, match, actions, 0)
            for i in range(self.c):
                match = parser.OFPMatch(eth_type=0x800,ipv4_dst = ("10.0.%d.%d"%(pos, i+2), "255.255.255.255"),\
                                                        ipv4_src = ("10.0.1.2"))
                actions = [parser.OFPActionOutput(i+self.s+1,0)]
                self.add_flow(datapath, 1, match, actions, 0)

        #add table 1
        match = parser.OFPMatch()
        actions = [parser.OFPActionOutput(1,0)]
        self.add_flow(datapath, 0, match, actions,1)
        for i in range(1,self.s+1):
            if i != pos:
                match = parser.OFPMatch(eth_type=0x800,ipv4_dst = ("10.0.%d.0"%i, "255.255.255.0"))
                actions = [parser.OFPActionOutput(i,0)]
                self.add_flow(datapath, 1, match, actions, 1)
        if pos == 1:
            match = parser.OFPMatch(eth_type=0x800,ipv4_dst = ("10.0.1.2", "255.255.255.255"))
            actions = [parser.OFPActionOutput(self.s+1,0)]
            self.add_flow(datapath, 1, match, actions, 1)
            
        else:
            for i in range(self.c):
                match = parser.OFPMatch(eth_type=0x800,ipv4_dst = ("10.0.%d.%d"%(pos, i+2), "255.255.255.255"))
                actions = [parser.OFPActionOutput(i+self.s+1,0)]
                self.add_flow(datapath, 2, match, actions, 1)
        
        match = parser.OFPMatch(eth_type=0x800,ipv4_dst = ("10.0.2.2", "255.255.255.255"))
        inst = [parser.OFPInstructionGotoTable(1)]
        mod = parser.OFPFlowMod(match=match, datapath=datapath, table_id=0, instructions=inst)
        datapath.send_msg(mod)

        match = parser.OFPMatch(eth_type=0x800,ipv4_dst = ("10.0.2.3", "255.255.255.255"))
        inst = [parser.OFPInstructionGotoTable(1)]
        mod = parser.OFPFlowMod(match=match, datapath=datapath, table_id=0, instructions=inst)
        datapath.send_msg(mod)